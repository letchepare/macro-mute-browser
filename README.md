# macro-mute-browser
# Project created in AHK to mute chrome.exe on press of Mouse Button 5 while valorant is running
The purpose is to cut music while playing in high pressure moments

# Requirements

This project requires:
- [AHK](https://www.autohotkey.com/) - Handles the HotKey
- [NIRCMD](https://www.nirsoft.net/utils/nircmd.html) - execute the mute/unmute action